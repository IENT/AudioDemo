classdef panning < handle
  
  
  properties
    hgui = struct();
    controller;
    tr;
    objects = [];
    objectAngles = [];
    objectVolumes = [];
    objectFXs = [];
    defaultVolume = 0.7;
    defaultColor  = [87 171 39]/255;
  end
  
  methods
    %% Constructor
    function obj = panning(controller,tr,viewer)
      % Set controller and translator
      obj.controller = controller;
      obj.tr = tr;
      
      % Setup GUI
      tmp = viewer.hgui.fig.OuterPosition;
      obj.hgui.fig = figure('Visible','on',...
         'Units','normalized','OuterPosition',[tmp(1)+tmp(3),tmp(2),0.25,tmp(4)],... % [0,0.1,0.5,0.5]
        'Name',[obj.tr('title') ' - ' obj.tr('Panning')],'NumberTitle','off',...
        'menuBar','none','CloseRequestFcn',@obj.CloseRequestHandler,...
        'WindowScrollWheelFcn',@obj.ObjectScrollHandler,'KeyPressFcn',@viewer.keyPressHandler); %'WindowButtonDownFcn',@obj.toggleObjectHighlight,
      
      obj.objects = gobjects(0);
      obj.hgui.mixSlots = gobjects(0);
      obj.hgui.ax = axes('Parent',obj.hgui.fig','XColor','none','YColor','none',...
        'Units','normalized','Position',[0 .45 1 .55]);
      
      dxPan = 0.01;
      obj.hgui.mix = uipanel('Title',obj.tr('mix'),'Parent',obj.hgui.fig,...
        'Units','normalized','Position',[dxPan 0.01 .7-2*dxPan .4]);
      
      obj.hgui.ctrl = uipanel('Title',obj.tr('ctrl'),'Parent',obj.hgui.fig,...
        'Units','normalized','Position',[sum(obj.hgui.mix.Position([1 3]))+2*dxPan 0.01 .3-2*dxPan .4]);
      
      obj.hgui.hrtf = uicontrol('Style','checkbox','String','HRTF',...
          'Units','normalized','Position',[0.1, 0.85 ,.5,0.1],...
          'Value',0,'Parent',obj.hgui.ctrl);
        
      obj.hgui.reset = uicontrol('Style','pushbutton','String','Reset',...
          'Units','normalized','Position',[0.1, 0.65 ,.5, 0.15],...
          'Parent',obj.hgui.ctrl,'callback',@obj.resetVolumes);
            
      obj.reset();
    end
    
    
    %% Reset objects
    function reset(obj)
      fig = obj.hgui.fig;
      ax = obj.hgui.ax;

      if ~isempty(obj.objects)
        for it=1:length(obj.objects)
          if isvalid(obj.objects(it))
            delete(obj.objects(it).UserData.text);
          end
        end
        delete(obj.objects);
      end

      cla(ax);
      hold(ax,'all');
      xlabel(ax,'');
      ylabel(ax,'');
      title(ax,'');
      
      % Number of objects
      Nobjects = obj.controller.model.getNumObjects();
      
      % Try to read labels
      [a,b] = fileparts(obj.controller.model.haudio.reader.Filename);
      fn = [a filesep b '.csv'];
      if exist(fn, 'file') == 2 && strcmp(obj.controller.model.data.dataFetchMode,'Reader')
        tmp=fileread(fn); labels=textscan(tmp,'%s','delimiter',','); labels=labels{1};
      else
        labels = cellfun(@num2str,num2cell(1:Nobjects),'unif',false)';
      end
        
      % Plot half-circle
      ang=0:0.01:pi;
      plot(ax,cos(ang),sin(ang),'Color',0.5*[1 1 1],'LineStyle','--')

      % Mark fixed positions
      azi = obj.controller.model.data.up.hrtf.allAzis;
      plot(ax,cos(azi/ 180 * pi),sin(azi/ 180 * pi),'o','Color',0.5*[1 1 1])
      
      % Plot object markers
      obj.objects = gobjects(1,Nobjects);
      
      % Spread objects over plane
      if Nobjects > 1
        ang0 = linspace(0,180,Nobjects);
        ang0 = interp1(azi,azi,ang0,'nearest');
        ang0 = fliplr(ang0);
      else
        ang0 = 90;
      end
      
      obj.objectAngles = ang0;
      obj.objectVolumes = ones(size(ang0));
      obj.objectFXs = false(size(ang0));
      
      rad = 0.075;
      % Plot objects
      for it = 1:1:Nobjects
          x0 = cos(ang0(it) / 180*pi); y0 = sin(ang0(it) / 180*pi);
          obj.objects(it) = rectangle('Position',[x0-rad y0-rad 2*rad 2*rad], ...
            'Curvature', [1 1], 'Parent',ax);
          obj.objects(it).FaceColor = obj.defaultColor;
          obj.objects(it).UserData.text = text(x0,y0-2*rad,labels{it},'FontSize',14,'Parent',ax,'HorizontalAlignment','center');
          obj.objects(it).UserData.rad  = rad;
          obj.objects(it).UserData.id = it;
          
          c = uicontextmenu;
          uimenu(c,'Label','Mute','Callback',@obj.toggleObjectHighlight);
          uimenu(c,'Label','Solo','Callback',@obj.toggleObjectHighlight);
          c.Parent = fig;
          c.UserData.id = it;
          
          obj.objects(it).UIContextMenu = c;
      end


      % Head
      rad2 = 0.1;
      rectangle('Position',[0-rad2 0-rad2 2*rad2 2*rad2], ...
            'Curvature', [1 1], 'Parent',ax);
      % Nose
      plot(ax,rad2*[0.2 0 -0.2],rad2*[0.95 1.4 0.95],'k');
      % Ears
      rectangle('Position', rad2*[1, -0.5, 0.2, 1], ...
        'Curvature', [1 1],'Parent', ax);
      rectangle('Position', rad2*[-1.2, -0.5, 0.2, 1], ...
        'Curvature', [1 1],'Parent', ax);

      % Axis stuff
      set(ax,'DataAspectRatio',[1 1 1],'Xlim',[-1.5 1.5],'YLim',[-0.5 1.5]);
      ax.XLim = [-1.5 1.5];
      draggable(obj.objects,'n',[-1.1 1.1 -0.1 1.1],@obj.draggableUpdateHandler,'endfcn',@obj.draggableEndHandler);
      
      ax.XTickLabel = {};
      ax.YTickLabel = {};
      box(ax, 'on');
      
      % Mixing control
      slotWidth=0.075;
      margin = (1-Nobjects*slotWidth)/Nobjects;
      if Nobjects*(slotWidth+margin) > 1, slotWidth=0.005; end
      
      volSliders = obj.hgui.mixSlots;
      if ~isempty(volSliders)
        for it=1:length(volSliders)
          delete(volSliders(it).UserData.fxActive);
          delete(volSliders(it).UserData.label);
          delete(volSliders(it).UserData.panelHandle);
        end
      end
      
      volSliders = gobjects(1,Nobjects);
      x = margin/2;
      for it=1:Nobjects
        % Volume slider
        [volSliders(it),~,edit] = ...
          sliderPanel('Parent',obj.hgui.mix,'Title','', ...
          'Units','normalized','Position', [x,0.1,slotWidth,0.8],...
          'Min',0, 'Max',1,'Value', .7, ...
          'bordertype','none','Labels','off','callback',@obj.updatePanningMixSlider);
        % Adjust size
        delta = 0.25;
        edit.Position(4) = edit.Position(4)-delta;
        volSliders(it).Position(2) = volSliders(it).Position(2)-delta;
        volSliders(it).Position(4) = volSliders(it).Position(4)+delta;
        
        % Activate filter
        volSliders(it).UserData.fxActive = uicontrol('Style','checkbox','String','Fx',...
          'Units','normalized','Position',[x, 0.85 ,slotWidth*2,0.1],...
          'Value',0,'Parent',obj.hgui.mix,'callback',@obj.updatePanningFx);
        volSliders(it).UserData.fxActive.UserData.id = it;
        
        volSliders(it).UserData.label = uicontrol('Style','text', 'String',labels{it},...
        'Units','normalized','Position',[x-slotWidth/2, 0.01 ,slotWidth*2,0.1],...
        'Parent',obj.hgui.mix,'HorizontalAlignment','center');
        
        volSliders(it).UserData.id = it;
      
        x = x + slotWidth+margin;
      end
      obj.hgui.mixSlots = volSliders;
      obj.resetVolumes();
      obj.controller.model.data.up.changeNumObjects(obj.getObjectAngles());
      
      % Hide figure if only one object
      if Nobjects > 1
        obj.hgui.fig.Visible = 'on';
      else
        obj.hgui.fig.Visible = 'off';
      end
      
    end
    
    function updatePanningMixSlider(obj,handles,event) %#ok<INUSD>
      vol = round(handles.Value*100)/100;
      obj.setObjectVolume(obj.objects(handles.UserData.id),vol);
    end
    
    function toggleObjectHighlight(obj, handles, event) %#ok<INUSD>
      id = handles.Parent.UserData.id;
      
      switch handles.Label
        case 'Solo'
          mask = zeros(size(obj.objects)); mask(id) = 1;
          if ~isequal(obj.getObjectVolume(),mask*1.0)
            % Highlight
            for n=1:length(mask)        
              obj.setObjectVolume(obj.objects(n), mask(n));
            end
          else
            % Undo highlight
            for n=1:length(obj.objects)
              obj.setObjectVolume(obj.objects(n), obj.defaultVolume);
            end
          end
        case 'Mute'
          vol = obj.getObjectVolume(id);
          if vol ~= 0
            obj.setObjectVolume(obj.objects(id), 0);
          else
            obj.setObjectVolume(obj.objects(id), obj.defaultVolume);
          end
      end
    end
    function updatePanningFx(obj,handles,event) %#ok<INUSD>
      obj.objectFXs(handles.UserData.id) = handles.Value;
      
    end
    % Calculating x and y for drawing the circle
    function draggableUpdateHandler(obj,objectHandle)
      pos = get(objectHandle,'Position');
      if ~isempty(pos)
        % Refine angle
        rad = objectHandle.UserData.rad;
        x0c = pos(1)+rad;
        ang = obj.convertObjectAngles(x0c);
        
        % Save angle
        obj.objectAngles(objectHandle.UserData.id) = ang;
        
        % Calculate new position
        x0 = cos(ang / 180*pi); y0 = sin(ang / 180*pi);
        objectHandle.Position(1) = x0-rad; objectHandle.Position(2) = y0-rad;
        objectHandle.UserData.text.Position(1)=x0; objectHandle.UserData.text.Position(2) = y0-rad*2;
      end
    end
    
    function draggableEndHandler(obj,handles,event) %#ok<INUSD>
      obj.controller.model.data.up.changeFilter(obj.getObjectAngles());
    end

    
    % Set volume of object
    function setObjectVolume(obj,objectHandle,vol)
      if vol > 1, vol = 1; end;
      if vol < 0, vol = 0; end
      
      % Set volume
      id = objectHandle.UserData.id;
      obj.objectVolumes(id) = vol;
      
      % Set color
      objectHandle.FaceColor = 1+vol*(obj.defaultColor-1);
      
      % Update mixing console
      obj.hgui.mixSlots(id).Value = vol; % Update slider
      obj.hgui.mixSlots(id).UserData.editHandle.String = num2str(vol); % Update text
      
    end
    
    function adjustObjectVolume(obj,objectHandle,increase)
      vol = obj.objectVolumes(objectHandle.UserData.id);
      step = 0.1;

      if increase
        vol = vol + step;
      else
        vol = vol - step;
      end
      vol = round(vol/step)*step;

      % Save volume and display color change
      obj.setObjectVolume(objectHandle,vol);
    end
    
    % Reset all volumes
    function resetVolumes(obj, handles,event) %#ok<INUSD>
      for it=1:length(obj.objects)
        obj.setObjectVolume(obj.objects(it), obj.defaultVolume);
      end
    end
    

    % Scroll handler adjusts volume of certain object
    function ObjectScrollHandler(obj,handles,event) %#ok<INUSL>
      % Get object
      c = obj.hgui.ax.CurrentPoint;
      ang = obj.convertObjectAngles(c(1));
      angs = obj.getObjectAngles();
      [~,currObj] = min(abs(angs-ang));
      
      % Adjust volume
      obj.adjustObjectVolume(obj.objects(currObj), event.VerticalScrollCount<0);
    end
    
    % Return angles of objects
    function angs = getObjectAngles(obj)
      angs = obj.objectAngles;
    end
    
    % Return volumes of objects
    function vol = getObjectVolume(obj,id)
      vol = obj.objectVolumes;
      if nargin > 1
        vol = vol(id);
      end
    end
    
    % Return fx active
    function mask = getObjectFX(obj,id)
      mask = obj.objectFXs;
      if nargin > 1
        mask = mask(id);
      end
    end
    
    % Convert angles of objects
    function ang = convertObjectAngles(obj,x)
      % Inverse cosine
      mask = x < -1; x(mask) = -1;
      mask = x >  1; x(mask) =  1;
      ang = acosd(x);
      
      % Return only valid values
      azi = obj.controller.model.data.up.hrtf.allAzis;
      ang = interp1(azi,azi,ang,'nearest');
    end
    
    %% Handlers
    % Close request
    function CloseRequestHandler(obj,handle,event) %#ok<INUSD>
      obj.hgui.fig.Visible = 'off';
    end
  end
  
end