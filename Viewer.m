classdef Viewer < handle
  
  properties
    controller;
    hgui = struct();
    plotLocked;
    limBuffer = {};
    NLimBuffer = 2;
    fMax;
    plotMode;
    tr = [];
    panning;
  end
  
  
  methods
    %% Constructor
    function obj = Viewer(ctr,tr)
      if nargin < 1, AudioDemo(); return; end
      % Set controller
      obj.controller = ctr;
      obj.tr = tr;
      
      % Lock plot
      obj.plotLocked = 1; 
      
      obj.fMax = 300; % in Hz
      obj.plotMode = obj.tr('pl:time');
      
      allFilterModes = {obj.tr('filt:lp') obj.tr('filt:hp') obj.tr('filt:bp') ...
        obj.tr('filt:comb') obj.tr('filt:wah') obj.tr('filt:crazy')};
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Figure
      % We want do distribute 5 panels (playpause, source, filter, sub
      % sampling and plot), but control does not need too much space.
      wPan = 0.225;
      
      dxPan = 0.005;
      dyPan = 0.05;
      wPanPlay = 1-4*wPan-6*dxPan;
      hPan = 1-2*dyPan;
      
      dxEl = 0.05;
      dyEl = 0.05;
      hEl = 0.25;
      wEl = 1-2*dxEl;
      wEll = 0.5-2*dxEl;
      wElr = 0.5-dxEl;
      hSl = hEl*1.25;
      
      obj.hgui.fig = figure('Units','normalized','OuterPosition',[0,0.1,0.5,0.5],...
        'Name',obj.tr('title'),'NumberTitle','off',...
        'menuBar','none','CloseRequestFcn',@obj.CloseRequestHandler,'KeyPressFcn',@obj.keyPressHandler);
      set(obj.hgui.fig,'Toolbar','figure');
      
      % Hite toolbar buttons
      hideTools = {'Plottools.PlottoolsOn' 'Plottools.PlottoolsOff' ...
        'Annotation.InsertLegend'  'DataManager.Linking' 'Exploration.Brushing' ...
        'Standard.EditPlot' 'Standard.PrintFigure'  'Standard.SaveFigure' ...
        'Standard.FileOpen' 'Standard.NewFigure'}; %,'Exploration.Rotate'
      for n=1:length(hideTools)
        t = findall(obj.hgui.fig,'tag',hideTools{n});
        if ~isempty(t)
          t.Visible = 'off';
        end
      end
      
      % Axes
      axPos = [0.075,0.4,0.9,0.55];
      obj.hgui.ax = axes('Parent',obj.hgui.fig,...
        'Units','normalized','Position',axPos);
      plot(0.01:0.01:0.05)
      xlabel('Dummy'); ylabel('Dummy'); title('Dummy');
      axPos = get(obj.hgui.ax,'OuterPosition');
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Main panel
      obj.hgui.mainPanel = uipanel('Parent',obj.hgui.fig,'FontSize',14,...
        'Units','normalized','Position',[dxPan 0.01 1-2*dxPan axPos(2)-0.01*2]);
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Play panel
      obj.hgui.playPanel = uipanel('Title',obj.tr('ctrl'),'Parent',obj.hgui.mainPanel,...
        'Units','normalized','Position',[dxPan dyPan wPanPlay hPan]);
      playPanPos = obj.hgui.playPanel.Position;
      
      obj.hgui.playPause = uicontrol('Style','pushbutton','String','Play','Tag','playPause',...
        'Units','normalized','Position',[dxEl,1-(dyEl+hEl),wEl, hEl],...
        'callback',@obj.ClickButtonHandler,'KeyPressFcn',@obj.keyPressHandler,...
        'Parent',obj.hgui.playPanel);
      obj.hgui.stop = uicontrol('Style','pushbutton','String','Stop','Tag','stop',...
        'Units','normalized','Position',[dxEl,1-2*(dyEl+hEl),wEl, hEl],...
        'callback',@obj.ClickButtonHandler,'KeyPressFcn',@obj.keyPressHandler,...
        'Parent',obj.hgui.playPanel);

      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Source panel      
      obj.hgui.sourceTab = uitabgroup(...
        'Units','normalized','Position',[dxPan+sum(playPanPos([1 3])) dyPan wPan hPan],...
        'SelectionChangedFcn',@obj.generatorTabChangedHandler,'Parent',obj.hgui.mainPanel);
      srcPanPos = obj.hgui.sourceTab.Position;
      
      % Source file
      obj.hgui.sourceFile = uitab(obj.hgui.sourceTab,'Title',obj.tr('file'));
      
      obj.hgui.sourceFileLoad = uicontrol('Style','pushbutton','String',obj.tr('load'),'Tag','load',...
        'Units','normalized','Position',[dxEl,1-(dyEl+hEl),wEl*0.45, hEl],...
        'callback',@obj.ClickButtonHandler,'KeyPressFcn',@obj.keyPressHandler,...
        'Parent',obj.hgui.sourceFile);
      obj.hgui.sourceFileLoaded = uicontrol('Style','text','Parent',obj.hgui.sourceFile,...
        'Units','normalized','Position',[dxEl, dyEl,1-2*dxEl,hEl*1.4],...
        'String','TODO: schnogoschnogobogo.wav sampling rate 44 100 Hz', 'HorizontalAlignment', 'left');
      obj.hgui.repeat = uicontrol('Style','checkbox', 'String',obj.tr('repeat'),'Value',1,...
        'Units','normalized','Position',[dxEl, 1-2*(dyEl+hEl),wEl,hEl],'Parent',obj.hgui.sourceFile);
      
      % Generator
      obj.hgui.sourceGenerator = uitab(obj.hgui.sourceTab,'Title',obj.tr('gen'));
      
      y = 1-(dyEl+hEl);
      obj.hgui.generatorModeLabel = uicontrol('Style','text', 'String',obj.tr('type'),...
        'Units','normalized','Position',[dxEl, y ,wEll,hEl],...
        'Parent',obj.hgui.sourceGenerator,'HorizontalAlignment','left');
      tmp = obj.hgui.generatorModeLabel.Position;
      obj.hgui.generatorMode = uicontrol('Style', 'popupmenu','String',{obj.tr('sig:sine'),obj.tr('sig:saw'),obj.tr('sig:noise')},...
        'Units','normalized','Position',[sum(tmp([1 3]))+dxEl,y,wElr,hEl],...
        'callback',@obj.generatorModeRequest, 'Parent',obj.hgui.sourceGenerator);
      
      y = y-hSl;
      obj.hgui.generatorAmpLabel = uicontrol('Style','text', 'String',obj.tr('ampl'),...
        'Units','normalized','Position',[dxEl, y,wEll,hEl],...
        'Parent',obj.hgui.sourceGenerator,'HorizontalAlignment','left');
      tmp = obj.hgui.generatorAmpLabel.Position;
      obj.hgui.generatorAmp = sliderPanel('Title','', ...
        'Units','normalized','Position', [sum(tmp([1 3]))+dxEl,y,wElr,hSl],...
        'Min',0, 'Max',1,'Value', 0.25, ...
        'bordertype','none','Labels','off','Parent',obj.hgui.sourceGenerator,...
        'callback',@obj.roundAmpSlider);
      
      y = y-hSl-dyEl;
      obj.hgui.generatorFreqLabel = uicontrol('Style','text', 'String',obj.tr('freq'),...
        'Units','normalized','Position', [dxEl, y,wEll,hEl],...
        'Parent',obj.hgui.sourceGenerator,'HorizontalAlignment','left');
      tmp = obj.hgui.generatorFreqLabel.Position;
      obj.hgui.generatorFreq = sliderPanel('Title','', ...
        'Position', [sum(tmp([1 3]))+dxEl,y,wElr,hSl],...
        'Min',400, 'Max',20000,'Value', 440, ...
        'bordertype','none','Labels','off','Parent',obj.hgui.sourceGenerator,...
        'callback',@obj.roundGenFreqSlider);
      
      % Recorder
      obj.hgui.sourceRecorder = uitab(obj.hgui.sourceTab,'Title','Recorder');
      y = 1-(dyEl+hSl);
      obj.hgui.recAmpLabel = uicontrol('Style','text',...
        'String',obj.tr('volume'),...
        'Units','normalized','Position',[dxEl, y,wEll,hEl],...
        'Parent',obj.hgui.sourceRecorder,'HorizontalAlignment','left');
      tmp = obj.hgui.recAmpLabel.Position;
      [obj.hgui.recAmpSlider,~,obj.hgui.recAmpEdit] = ...
        sliderPanel('Parent',obj.hgui.sourceRecorder,'Title','', ...
        'Units','normalized','Position', [sum(tmp([1 3]))+dxEl,y,wElr,hSl],...
        'Min',0, 'Max',1,'Value', 0.5, ...
        'bordertype','none','Labels','off');
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Filter Panel
      obj.hgui.filterPanel = uipanel('Title',obj.tr('filt'),'Parent',obj.hgui.mainPanel,...
        'Units','normalized','Position', [dxPan+sum(srcPanPos([1 3])) dyPan wPan hPan]);
      filtPanPos = obj.hgui.filterPanel.Position;
      
      y = 1-(dyEl+hEl);      
      obj.hgui.filterActive = uicontrol('Style','checkbox','String',obj.tr('activate'),...
        'Units','normalized','Position',[dxEl, y ,wEll,hEl],...
        'Value',0,'callback',@obj.toggleFilterMode,'Parent',obj.hgui.filterPanel); 
      tmp = obj.hgui.filterActive.Position;
      obj.hgui.filterMode = uicontrol('Style', 'popupmenu','String',allFilterModes,...
        'Units','normalized','Position',[sum(tmp([1 3]))+dxEl,y,wElr,hEl],...
        'callback',@obj.filterModeChangedHandler,'Parent',obj.hgui.filterPanel);
      
      y = y-hSl;
      obj.hgui.f0Label = uicontrol('Style','text',...
        'String',obj.tr('centfreq'),...
        'Units','normalized','Position',[dxEl, y,wEll,hEl],...
        'Parent',obj.hgui.filterPanel,'HorizontalAlignment','left');
      tmp = obj.hgui.f0Label.Position;
      [obj.hgui.f0Slider,~,obj.hgui.f0Edit] = ...
        sliderPanel('Parent',obj.hgui.filterPanel,'Title','', ...
        'Units','normalized','Position', [sum(tmp([1 3]))+dxEl,y,wElr,hSl],...
        'Min',100, 'Max',3000,'Value', 1000, ...
        'callback',@obj.updateFilterSliderHandler,'bordertype','none','Labels','off');
      
      y = y-hSl-dyEl;
      obj.hgui.f1Label = uicontrol('Style','text','String',obj.tr('bandwidth'),...
        'Units','normalized','Position', [dxEl, y,wEll,hEl],...
        'Parent',obj.hgui.filterPanel,'HorizontalAlignment','left');
      tmp = obj.hgui.f1Label.Position;
      [obj.hgui.f1Slider,~,obj.hgui.f1Edit] = ...
        sliderPanel('Parent',obj.hgui.filterPanel,'Title','', ...
        'Units','normalized','Position', [sum(tmp([1 3]))+dxEl,y,wElr,hSl],...
        'Min',3, 'Max',100,'Value', 5, ...
        'callback',@obj.updateFilterSliderHandler,'bordertype','none','Labels','off');
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Sub sampling panel
      obj.hgui.subsampPanel = uipanel('Title',obj.tr('subsamp'),'Parent',obj.hgui.mainPanel,...
        'Units','normalized','Position', [dxPan+sum(filtPanPos([1 3])) dyPan wPan hPan]);
      subsampPos = obj.hgui.subsampPanel.Position;
      
      y = 1-(dyEl+hSl);  
      obj.hgui.decimationLabel = uicontrol('Style','text','String',obj.tr('subsampfac'),...
        'Units','normalized','Position',[dxEl, y,wEll,hEl],...
        'Parent',obj.hgui.subsampPanel,'HorizontalAlignment','left');
      tmp = obj.hgui.decimationLabel.Position;
      SliderMax = 32;
      [obj.hgui.decimationSlider,~,obj.hgui.decimationEdit] = ...
        sliderPanel('Parent',obj.hgui.subsampPanel,'Title','', ...
        'Units','normalized','Position', [sum(tmp([1 3]))+dxEl,y,wElr,hSl],...
        'Min',1, 'Max',SliderMax,'Value', 1,'SliderStep',[1/(SliderMax-1) 1/(SliderMax-1)], ...
        'callback',@obj.updateDecimation,'bordertype','none','Labels','off');
      
      y = y-hEl-dyEl;
      obj.hgui.frequencyLabel = uicontrol('Parent',obj.hgui.subsampPanel,'Style','text',...
        'String',obj.tr('originalfs'),'HorizontalAlignment','left',...
        'Units','normalized','Position',[dxEl, y,1-2*dxEl,hEl]); 
            
      y = y-hEl*0.75-dyEl;
      obj.hgui.compDecimation = uicontrol('Parent',obj.hgui.subsampPanel,'Style','checkbox', ...
        'Value',0,'String',obj.tr('loudnessComp'),...
        'Units','normalized','Position',[dxEl, y,1-2*dxEl,hEl]*0.75);  
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Plot panel
      obj.hgui.plotPanel = uipanel('Title',obj.tr('pl'),'Parent',obj.hgui.mainPanel,...
        'Units','normalized','Position',[dxPan+sum(subsampPos([1 3])) 1-dyPan-hPan*0.6 wPan hPan*0.6]);
      
      y = 1-(dyEl+2*hEl); 
      obj.hgui.plotModeLabel = uicontrol('Style','text','String',obj.tr('type'),...
        'Units','normalized','Position',[dxEl, y,wEll,hEl*2],...
        'Parent',obj.hgui.plotPanel,'HorizontalAlignment','left');
      tmp = obj.hgui.plotModeLabel.Position;
      obj.hgui.plotMode = uicontrol('Style', 'popupmenu','String',{obj.tr('pl:time'),obj.tr('pl:spectr'),obj.tr('pl:specgr')},...
        'Units','normalized','Position',[sum(tmp([1 3]))+dxEl,y,wElr,hEl*2],...
        'callback',@obj.changePlotMode,'Parent',obj.hgui.plotPanel);
      
      y = y-1.5*hEl-dyEl; 
      obj.hgui.fMaxLabel = uicontrol('Parent',obj.hgui.plotPanel,'Style','text',...
        'String',obj.tr('maxfreq'),'HorizontalAlignment','left',...
        'Units','normalized','Position',[dxEl, y, wEll, hEl*2]);
      tmp = obj.hgui.plotModeLabel.Position;
      [obj.hgui.fMax,~,obj.hgui.fMaxEdit] = sliderPanel('Parent',obj.hgui.plotPanel,'Title','', ...
        'Position', [sum(tmp([1 3]))+dxEl,y+0.01,wElr,hSl*1.75],...
        'Min',500, 'Max',20000,'Value', 5000, ...
        'Callback', @obj.updateFMax,'bordertype','none','Labels','off');
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Misc panel
      wEl1 = 0.25;
      obj.hgui.miscPanel = uipanel('Parent',obj.hgui.mainPanel,...
        'Units','normalized','Position',[dxPan+sum(subsampPos([1 3])) dyPan wPan hPan*0.3]);
      
      obj.hgui.panningButton = uicontrol('Parent',obj.hgui.miscPanel,'Style','pushbutton',...
        'Tag','panning','String',obj.tr('Panning'),'Units','normalized','Position',[wEl1*0+dxEl/2, dyEl,wEl1, 1-2*dyEl],...
        'callback',@obj.ClickButtonHandler,'KeyPressFcn',@obj.keyPressHandler);
      
      obj.hgui.about = uicontrol('Parent',obj.hgui.miscPanel,'Style','pushbutton',...
        'Tag','about','String',obj.tr('about'),'Units','normalized','Position',[wEl1*1+dxEl*1.5, dyEl,wEl1, 1-2*dyEl],...
        'callback',@obj.ClickButtonHandler,'KeyPressFcn',@obj.keyPressHandler);
      obj.hgui.aboutfig = [];
      
      
      % Panning Fig
      obj.panning = panning(ctr, obj.tr, obj);
            
      % Update filters
      obj.hgui.filter = struct();
      
      % Create limits buffer
      obj.limBuffer = zeros(obj.NLimBuffer,1);
      
      % Update fMax
      obj.updateFMax();
      
      % UpdateDecimation
      obj.updateDecimation();
      
      % Update filter panel
      obj.toggleFilterMode();
      
      % Unlock plot
      obj.plotLocked = 0;
    end
    
    
    %% Enable or disable filter
    function toggleFilterMode(obj, handles, event) %#ok<INUSD>      
      if ~obj.getFilterActive()
        enableMode = 'off';
      else
        enableMode = 'on';
      end
      
      % Enable/disable corresponding GUI elements
      obj.hgui.filterMode.Enable = enableMode;
      obj.hgui.filterModeLabel.Enable = enableMode;
      fn = { 'filterMode' 'filterModeLabel' 'f0Label' 'f0Edit' 'f0Slider' 'f1Label' 'f1Edit' 'f1Slider'};
      for iter=1:length(fn)
        tmp = fn{iter};
        tmp = obj.hgui.(tmp);
        tmp.Enable = enableMode;
      end
      
      % Update filter plot
      obj.updateFilterPlot();
      
      % Update panning mixing panel only if only one object is present
      if length(obj.panning.objects) == 1 
        if obj.getFilterActive(), tmp=1; else, tmp=0; end
        obj.panning.hgui.mixSlots(1).UserData.fxActive.Value = tmp;
        obj.panning.objectFXs(1) = tmp;
      end
    end
    
    
    %% Update plot
    function updatePlot(obj,s)
      if ~obj.plotLocked
        % Cut spectral data
        switch obj.plotMode
          case {obj.tr('pl:specgr'),obj.tr('pl:spectr')}
            s = s(1:obj.fMax,:);
        end
        
        % Axes limits and plot
        if isvector(s)
          % Calculate new limits
          mins = min(s(:)); mins = mins-0.1*abs(mins);
          maxs = max(s(:)); maxs = maxs+0.1*abs(maxs);
          
          % Append new limits
          obj.limBuffer(1) = min(obj.limBuffer(1),mins);
          obj.limBuffer(2) = max(obj.limBuffer(2),maxs);
          ylims = obj.limBuffer;

          % Special cases for limits
          ylims(1) = min(ylims(1),0);
          if diff(ylims) < eps
            ylims(1) = ylims(1)-0.1;
            ylims(2) = ylims(2)+0.1;
          end
        end
        
        switch obj.plotMode
          case obj.tr('pl:specgr')
            % Update plot
            obj.hgui.plot.CData = s;
          case obj.tr('pl:time')
            obj.hgui.plot.YData = s;
            obj.hgui.ax.YLim = ylims;
            
          case obj.tr('pl:spectr')
            obj.hgui.plot.YData = s;
            obj.hgui.ax.YLim = ylims;
            
            % Update filter plot limits
            if obj.getFilterActive() %&& ~strcmp(obj.getFilterData(),obj.tr('filt:hrtf'))
              targetVal = 0.8*max(ylims);
              currentVal = max(obj.hgui.plotFilter.YData);
              
              if abs(currentVal-targetVal) > eps
                obj.hgui.plotFilter.YData = obj.hgui.plotFilter.YData/currentVal*targetVal;
              end
            end
        end
      end
    end
    
    %% Update filter plot
    function updateFilterPlot(obj)
        
      if ~obj.plotLocked
        switch obj.plotMode
          case obj.tr('pl:spectr') % spectrum
            if obj.getFilterActive() %&& ~strcmp(obj.getFilterData(),obj.tr('filt:hrtf')) && ~strcmp(obj.getFilterData(),obj.tr('filt:crazy'))
              % Get transfer function
              dirac = zeros(size(obj.controller.model.data.streamBuffer));
              dirac(1) = 1;
              h = filter(obj.controller.model.data.b,obj.controller.model.data.a,dirac);
              H = abs(fft(h));
              H = H(1:length(obj.hgui.plotFilter.YData));
              
              % Display transfer function
              ylim = max(obj.hgui.ax.YLim);
              obj.hgui.plotFilter.YData = 0.8*ylim*H;
              obj.hgui.plotFilter.LineStyle = '-.';
            else
              obj.hgui.plotFilter.LineStyle = 'none';
            end
        end
      end
    end
    
    
    %% Reset
    function reset(obj)
      obj.hgui.playPause.String = 'Play';
      obj.hgui.sourceFileLoad.Enable = 'on';
      obj.limBuffer = zeros(size(obj.limBuffer));
      
      % Reset
      obj.panning.reset();
      
      % Reset filter
      obj.filterModeChangedHandler();
    end
    
    
    %% Button handler
    function ClickButtonHandler(obj, handles, event) %#ok<INUSD>
      % Get mode by button tag
      button = handles.Tag;
      switch button
        case 'playPause'
          obj.controller.playPause();
          
        case 'stop'
          obj.controller.reset();
          
        case 'load'
          % Get path
          [FileName,PathName] = uigetfile(fullfile(pwd,'wav','*.wav'),obj.tr('select'));
          
          % Publish path
          if FileName ~= 0
            obj.controller.loadFile(FileName,PathName);
          end
          
        case 'about'
          obj.createAboutFig();
          
        case 'panning'
          if strcmp(obj.panning.hgui.fig.Visible, 'on')
            obj.panning.hgui.fig.Visible = 'off';
          else
            tmp = obj.hgui.fig.OuterPosition;
            obj.panning.hgui.fig.OuterPosition(1) = tmp(1)+tmp(3);
            obj.panning.hgui.fig.OuterPosition(2) = tmp(2);
            obj.panning.hgui.fig.Visible = 'on';
          end
      end
    end
    
    
    %% Getter/setter
    function val = getFilterActive(obj)
      val =  obj.hgui.filterActive.Value;
    end
    
    function val = getRepeat(obj)
      val = obj.hgui.repeat.Value;
    end
    
    function updateFMax(obj,handle,event) %#ok<INUSD>
      tmpVal = round(obj.hgui.fMax.Value/100)*100;
      tmpVal = min(tmpVal,obj.hgui.fMax.Max);
      tmpVal = max(tmpVal,obj.hgui.fMax.Min);
      obj.hgui.fMax.Value = tmpVal;
      Fs = obj.controller.getFS();
      
      fMaxHz = obj.hgui.fMax.Value;
      if fMaxHz < 500, fMaxHz = 500; end
      if fMaxHz >= Fs/2, fMaxHz = Fs/2; end
      obj.hgui.fMax.String = num2str(fMaxHz);
      obj.fMax = round(fMaxHz/Fs*size(obj.controller.model.data.streamBuffer,1));
      obj.changePlotMode();
    end
    
    
    %% Change filter mode
    function filterModeChangedHandler(obj,handle,event) %#ok<INUSD>
      filterModeNew = getFilterData(obj);
      switch filterModeNew
        case {obj.tr('filt:lp'),obj.tr('filt:hp')} % Lowpass, highpass
          f0MinMaxVal = [400,20000,3000];
          f1MinMaxVal = [];
          f0Label = obj.tr('cutfreq');
          f1Label = '';
        case obj.tr('filt:crazy') % HRTF
          f0MinMaxVal = [];
          f1MinMaxVal = [];
          f0Label = '';
          f1Label = '';
        case obj.tr('filt:comb') % Comb filter
          f0MinMaxVal = [100 3000 1000];
          f1MinMaxVal = [3 100 5];
          f0Label = obj.tr('centfreq');
          f1Label = 'Q';
        case obj.tr('filt:bp') % Band path
          f0MinMaxVal = [500 16000 2000];
          f1MinMaxVal = [200 5000 500];
          f0Label = obj.tr('centfreq');
          f1Label = obj.tr('bandwidth');
        case obj.tr('filt:wah')
          f0MinMaxVal = [0,1,.5];
          f1MinMaxVal = [];
          f0Label = obj.tr('filt:wah');
          f1Label = '';
      end
      
      % First input
      if isempty(f0MinMaxVal)
        visibleMode = 'off';
      else
        visibleMode = 'on';
        obj.hgui.f0Label.String = f0Label;
        obj.hgui.f0Edit.String = num2str(f0MinMaxVal(3));
        obj.hgui.f0Slider.Min = f0MinMaxVal(1); obj.hgui.f0Slider.Max = f0MinMaxVal(2);
        obj.hgui.f0Slider.Value = f0MinMaxVal(3);
      end
      obj.hgui.f0Label.Visible = visibleMode;
      obj.hgui.f0Edit.Visible = visibleMode;
      obj.hgui.f0Slider.Visible = visibleMode;
      
      % Second input
      if isempty(f1MinMaxVal)
        visibleMode = 'off';
      else
        visibleMode = 'on';
        obj.hgui.f1Label.String = f1Label;
        obj.hgui.f1Edit.String = num2str(f1MinMaxVal(3));
        obj.hgui.f1Slider.Min = f1MinMaxVal(1); obj.hgui.f1Slider.Max = f1MinMaxVal(2);
        obj.hgui.f1Slider.Value = f1MinMaxVal(3);
      end
      obj.hgui.f1Label.Visible = visibleMode;
      obj.hgui.f1Edit.Visible = visibleMode;
      obj.hgui.f1Slider.Visible = visibleMode;
      
      % Update controller
      obj.controller.updateFilter();
    end
    
    
    %% Update decimation
    function updateDecimation(obj,handle,event) %#ok<INUSD>
      dec = round(obj.hgui.decimationSlider.Value);
      obj.hgui.decimationSlider.Value = dec;
      obj.controller.setDecimation(dec);
      Fs = obj.controller.model.getFS();
      obj.hgui.frequencyLabel.String = sprintf('%s: %.5g Hz\n%s:   %.5g Hz', obj.tr('originalfs'),Fs,obj.tr('newfs'),Fs/dec);
    end
    
    
    %% Change plot
    function changePlotMode(obj,handles,event) %#ok<INUSD>
      % Lock plot
      obj.plotLocked = 1;
      modeChanged = ~strcmp(obj.plotMode,obj.hgui.plotMode.String{obj.hgui.plotMode.Value});
      obj.plotMode = obj.hgui.plotMode.String{obj.hgui.plotMode.Value};
      % Create new plot handle
      Fs = obj.controller.getFS();
      NI = obj.controller.model.getNyquistIndex();

      fAxis = linspace(0,1,NI)*(Fs/2)/1000; % kHz
      fAxis = fAxis(1:obj.fMax);
      
      switch obj.plotMode
        case obj.tr('pl:specgr') % spectrogram
          if modeChanged
            SliderValues = [100 5000 2000];
          else
            SliderValues = [obj.hgui.fMax.Min obj.hgui.fMax.Max obj.hgui.fMax.Value];
          end
          hs = obj.controller.htimer.plotter.Period;
          tAxis = hs*(0:(size(obj.controller.model.data.streamSTFT,2)-1));
          
          x{1} = fAxis;
          x{2} = tAxis;
          obj.hgui.plot = imagesc(x{end:-1:1}, eps*ones(length(x{1})),...
            'Parent',obj.hgui.ax);
          obj.hgui.ax.YDir = 'normal';
          xlim(obj.hgui.ax, [tAxis(1) floor(tAxis(end)*10)/10])
          xlabel(obj.hgui.ax, obj.tr('time')); ylabel(obj.hgui.ax, obj.tr('freq'))
          title(obj.hgui.ax, obj.tr('pl:specgr'))
          grid(obj.hgui.ax,'on');
          obj.hgui.ax.XTickLabel = obj.hgui.ax.XTickLabel(end:-1:1);
          obj.hgui.fMax.Enable = 'on'; obj.hgui.fMax.UserData.editHandle.Enable = 'on'; obj.hgui.fMaxLabel.Enable = 'on';
          
        case obj.tr('pl:spectr') % spectrum
          if modeChanged
            SliderValues = [100 Fs/2 Fs/2];
          else
            SliderValues = [obj.hgui.fMax.Min obj.hgui.fMax.Max obj.hgui.fMax.Value];
          end
          
          obj.hgui.plotFilter = plot(fAxis, zeros(length(fAxis),1),...
            'Parent',obj.hgui.ax, 'LineStyle','none','Color','r');
          obj.hgui.plot = line(fAxis, zeros(length(fAxis),1),...
            'Parent',obj.hgui.ax);
          xlim(obj.hgui.ax, [fAxis(1) fAxis(end)]); ylim(obj.hgui.ax, [0,20]);
          xlabel(obj.hgui.ax, obj.tr('freq')); ylabel(obj.hgui.ax, obj.tr('ampl'))
          title(obj.hgui.ax, obj.tr('pl:spectr'))
          grid(obj.hgui.ax,'on');
          obj.hgui.fMax.Enable = 'on'; obj.hgui.fMax.UserData.editHandle.Enable = 'on';  obj.hgui.fMaxLabel.Enable = 'on';
          
        case obj.tr('pl:time')
          if modeChanged
            SliderValues = [100 Fs/2 Fs/2];
          else
            SliderValues = [obj.hgui.fMax.Min obj.hgui.fMax.Max obj.hgui.fMax.Value];
          end
          L = length(obj.controller.model.data.streamBuffer);
          tAxis = (0:(L-1))/Fs;
          obj.hgui.plot = plot(tAxis, zeros(L,1),...
            'Parent',obj.hgui.ax);
          
          xlim(obj.hgui.ax, [tAxis(1),tAxis(end)]); ylim(obj.hgui.ax, [-1 1]);
          xlabel(obj.hgui.ax, obj.tr('time')); ylabel(obj.hgui.ax, obj.tr('ampl'))
          title(obj.hgui.ax, obj.tr('timesignal'))
          grid(obj.hgui.ax,'on');
          obj.hgui.ax.XTickLabel = obj.hgui.ax.XTickLabel(end:-1:1);
          obj.hgui.fMax.Enable = 'off'; obj.hgui.fMax.UserData.editHandle.Enable = 'off'; obj.hgui.fMaxLabel.Enable = 'off'; 
      end
      obj.limBuffer = zeros(size(obj.limBuffer));
      if obj.hgui.fMax.Min ~= SliderValues(1) || obj.hgui.fMax.Max ~= SliderValues(2) || obj.hgui.fMax.Value ~= SliderValues(3)
          obj.hgui.fMax.Min = SliderValues(1);
          obj.hgui.fMax.Max = SliderValues(2);
          obj.hgui.fMax.Value = SliderValues(3);
          obj.hgui.fMaxEdit.String = num2str(SliderValues(3));
          obj.updateFMax();
      end
      
      % Unlock plotting
      obj.plotLocked = 0;
      obj.updateFilterPlot();
    end
    
    function updateFilterSliderHandler(obj,handles,event) %#ok<INUSD>
      if ~strcmp(obj.getFilterData(),obj.tr('filt:wah'))
        obj.hgui.f0Slider.Value = round(obj.hgui.f0Slider.Value);
        obj.hgui.f1Slider.Value = round(obj.hgui.f1Slider.Value);
      else
        obj.hgui.f0Slider.Value = round(obj.hgui.f0Slider.Value*100)/100;
      end
      obj.controller.updateFilter();
    end
    
    function vol = getRecVolume(obj)
      vol = obj.hgui.recAmpSlider.Value;
    end
    
    function generatorTabChangedHandler(obj,handles,event) %#ok<INUSD>
      switch handles.SelectedTab.Title
        case obj.tr('gen')
          tmp = obj.hgui.generatorMode.String{obj.hgui.generatorMode.Value};
        case obj.tr('file')
          tmp = 'Reader';
        case 'Recorder'
          tmp = 'Recorder';
      end
      
      obj.controller.setDataFetchMode(tmp);
      obj.panning.reset();
      obj.controller.updateFilter();
    end
    
    function generatorModeRequest(obj,handles,event) %#ok<INUSD>
      genMode = handles.String{handles.Value};
      switch genMode
        case obj.tr('sig:noise')
          enableMode = 'off';
        case {obj.tr('sig:sine') obj.tr('sig:saw')}
          enableMode = 'on';
      end
      obj.hgui.generatorFreq.Enable = enableMode;
      obj.hgui.generatorFreq.UserData.editHandle.Enable = enableMode;
      obj.hgui.generatorFreqLabel.Enable = enableMode;
      obj.controller.setDataFetchMode(genMode);
    end
    
    
    %% Return data important for filter
    function [filterMode,f0,f1] = getFilterData(obj,Fs) %#ok<INUSD>
      
      % Get GUI values 
      filterMode = obj.hgui.filterMode.String{obj.hgui.filterMode.Value};
      
      if nargout > 1
        f0 = obj.hgui.f0Slider.Value;
        f1 = obj.hgui.f1Slider.Value;

        % Validate
        switch filterMode
          case obj.tr('filt:bp') % bandpass
            if f0-f1/2 < 100
              f0 = f1/2+100;
              obj.hgui.f0Slider.Value = f0; obj.hgui.f0Edit.String = num2str(f0);
            end
        end
      end
    end
    
    
    function [amp,f] = getGeneratorData(obj,Fs) %#ok<INUSD>
      if isfield(obj.hgui,'generatorAmp') && ~isempty(obj.hgui.generatorAmp)
        amp = (obj.hgui.generatorAmp.Value); 
      else
        amp = 1;
      end
      if isfield(obj.hgui,'generatorFreq') && ~isempty(obj.hgui.generatorFreq)
        f = obj.hgui.generatorFreq.Value;
      else
        f = 1e3;
      end   
    end
    
    %% Display file info
    function displayFileInfo(obj,FileName,Fs)
      obj.hgui.sourceFileLoaded.String = sprintf('%s, %s %d Hz.',FileName,obj.tr('fs'),Fs);
    end
    
    function roundAmpSlider(obj,handle,event) %#ok<INUSD>
      obj.hgui.generatorAmp.Value = round(20*obj.hgui.generatorAmp.Value)/20;
    end
    
    function roundGenFreqSlider(obj,handle,event) %#ok<INUSD>
      obj.hgui.generatorFreq.Value = round(obj.hgui.generatorFreq.Value);
    end
    
    
    %% About fig
    function createAboutFig(obj)
      if isempty(obj.hgui.aboutfig)
        obj.hgui.aboutfig = figure('Menubar','none','NumberTitle','off',...
          'Units','normalized','Position',[0.4,0.4,0.2,0.2],...
          'Name',obj.tr('about'),'CloseRequestFcn',@obj.AboutFigCloseRequestHandler);
        ax = axes('Parent',obj.hgui.aboutfig','XColor','none','YColor','none',...
          'Units','normalized','Position',[0,0.5,1,0.5]);
        [I,~,mask]=imread('data/rwth_ient_logo.png');
        logo = imshow(I,'Parent',ax);
        logo.AlphaData = mask;
        mainText = {
          '© Lehrstuhl und Institut für Nachrichtentechnik 2021'
          'RWTH Aachen University'
          ' '
          'Christian Rohlfing'
        };
        uicontrol('Parent',obj.hgui.aboutfig,...
          'Units','normalized','Style', 'text','HorizontalAlignment','left',...
          'String', mainText,'FontSize',11,'FontName','Arial',...
          'Position',[0.05,0.1,0.9,0.4]);
      end
    end
    function AboutFigCloseRequestHandler(obj,handle,event) %#ok<INUSD>
      delete(obj.hgui.aboutfig);
      obj.hgui.aboutfig = [];
    end
    
    % Update filter
    function updateFilter(obj,handles,event) %#ok<INUSD>
      obj.controller.updateFilter();
    end
    
    
    % Close request
		function CloseRequestHandler(obj, handles, event) %#ok<INUSD>
      % Delete controller
      obj.controller.CloseRequestHandler();
      
      % Delete figure
      delete(obj.hgui.fig);
      
      % Delete panning figure
      delete(obj.panning.hgui.fig);
      
      obj.AboutFigCloseRequestHandler();
    end
    
    
    % Handler for keyboard events
    function keyPressHandler(obj,handle,event) %#ok<INUSL>
      switch event.Key
        case {'escape','return'}
          obj.CloseRequestHandler();
        case 'space'
          obj.controller.playPause();
      end
    end
    
    
    %% Displays status text
    function displayStatus(obj,text)
      obj.hgui.statusText.String = text;
%       if ~isempty(obj.htimer.status)
%         if strcmp(obj.htimer.status.Running,'on')
%           stop(obj.htimer.status);
%         end
%         delete(obj.htimer.status);
%       end
%       
%       obj.htimer.status = timer('StartDelay',3,'UserData',obj.hgui.statusText,...
%         'TimerFcn',@(timerObj,~)(set(timerObj.UserData,'String', '')));
%       start(obj.htimer.status);
    end
    
  end % methods 
end % Viewer

