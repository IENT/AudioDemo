classdef upConvOld < handle
% UPCONV class for uniformed-partitioned convolution

  properties
    hrtf = struct(); % complete set of HRTFs
    data = struct(); % current data
  end
  
  methods
    %% Constructor
    function obj = upConvOld(bufferSize,room)
      % Load room impulse response
      switch room
        case 'stairway'
          tmp = load('data/combined_stairway.mat');
          radSel = 1;
        case 'aula'
          tmp = load('data/combined_aula_carolina.mat');
          radSel = ':';
      end
      N = size(tmp.H,1);
      H = 10*squeeze(tmp.H(1:N,:,radSel,:));      
      obj.hrtf.H = flip(H,3);
      obj.hrtf.allAzis = tmp.azi;
      
      % Initialize uniformed partitioned convolution
      obj.init(bufferSize,90);
      
    end
    
    %% Initialize with bufferlength B and azimuth value azi
    function init(obj,B,azis)
      h = obj.hrtf.H(:,:,obj.hrtf.allAzis == azis(1));
      up = struct();
      up.C = size(h,2); % number of channels
      up.K = 2*B; % FFT length
      up.L = up.K-B; % Length of one filter segment
      up.NI = up.K/2+1;  % Nyquist index
      
      % Buffer
      up.buffer = zeros(up.K,up.C);
      up.oldAzi = 361;
      
      up.NH = size(h,1); % Filter length
      if up.L > up.NH
        up.L = up.NH;
      end
      up.P = ceil(size(h,1)/up.L); % Number of filter segments
      up.Ninval = up.K-B; % Number of valid samples of ys
      up.Nval   = B;      % Number of invalid samples of ys
      
      up.circshiftInd = mod((0:up.P-1)-1, up.P)+1; % helper index vector for updading frequency delay line
      up.Fdl = zeros(up.NI, up.P, up.C); % Frequency delay line
      up.updateVol = 0;
      
      obj.data.up = {up};      
      obj.changeNumObjects(azis);
    end
    
    
    %% Update number of objects
    function changeNumObjects(obj,azis)
      obj.data.up{1}.oldAzi = 361;
      obj.data.up = repmat(obj.data.up(1),1,length(azis));
      obj.data.oldUp = obj.data.up;
      
      obj.changeFilter(azis);
    end
    
    %% Change filter values for uniformed partitioned convolution
    function changeFilter(obj,azis)
      for it=1:length(azis)
        if azis(it) ~= obj.data.up{it}.oldAzi % Update only if object's position changed
          
          obj.data.oldUp{it} = obj.data.up{it};
          obj.data.up{it}.updateVol = 1;
          
          obj.data.up{it} = obj.changeSingleFilter(azis(it),obj.data.up{it});
          obj.data.up{it}.oldAzi = azis(it);
        end
      end
    end
    
    % Change filter values for one transfer path
    function up = changeSingleFilter(obj,azi,up)
      h = obj.hrtf.H(:,:,obj.hrtf.allAzis == azi);
      if up.P*up.L-up.NH > 0
        h = [h; zeros(up.P*up.L-up.NH,size(h,2))];
      end

      hp = reshape(h, up.L, up.P, up.C);

      % Transform filter
      up.Hp = fft(hp,up.K);
      up.Hp = up.Hp(1:up.NI,:,:);
    end
    
    
    %% Convolution
    % Maps N objects in s to 2 ouput channels in ys
    function ys = conv(obj,s,vol)
      N = size(s,2);
      
      ys = 0;
      for it=1:N
        if vol(it) > 0 % be lazy
          % Filter each object
          [tmp,obj.data.up{it}] = obj.convSingle(s(:,it),obj.data.up{it});
          
          % Handle filter update
          volOld = obj.data.up{it}.updateVol;
          if volOld > 0
            % Filter input with old filter states
            [tmpOld,obj.data.oldUp{it}] = obj.convSingle(s(:,it),obj.data.oldUp{it});
            
            % Add the two outputs together
            tmp = volOld*tmpOld + (1-volOld)*tmp;
            obj.data.up{it}.updateVol = volOld - 0.2; disp(obj.data.up{it}.updateVol)
            if obj.data.up{it}.updateVol < eps, obj.data.up{it}.updateVol = 0; end
          end
          
          % Add current object stream to output
          ys = ys + vol(it)*tmp;
        end
      end
    end
    
    
    % Convolution of single transfer path
    % Maps 1 object in s to 2 ouput channels in ys
    function [ys,up] = convSingle(obj,s,up) %#ok<INUSL>

      % Update buffer
      up.buffer = [up.buffer(up.Ninval+1:end,:); [s,s]];
      xs = up.buffer;

      % Transform segment
      Xs = fft(xs, up.K);
      if length(xs) == 1, Xs = Xs';  end
      Xs = Xs(1:up.NI,:);

      % Circular shift and update frequency delay line
      up.Fdl = up.Fdl(:,up.circshiftInd,:);
      up.Fdl(:,1,:) = Xs;

      % Multiplication and addition in the frequency domain
      tmp = up.Fdl .* up.Hp;
      Ys = squeeze(sum(tmp,2));

      % ISTFT
      ys = ifft([Ys;conj(Ys(end-1:-1:2,:,:))], up.K, 'symmetric');

      if length(ys(:)) == 1, ys = zeros(up.K,1); end
      % Crop output
      m_start = up.Ninval+1; % Forget invalid samples
      m_stop  = m_start + up.Nval-1;

      ys = ys(m_start:m_stop,:);
    end
  end
  
end