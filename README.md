<img align="right" src="https://git.rwth-aachen.de/IENT/ient.nb/raw/master/figures/rwth_ient_logo@2x.png" alt="Logo Institut für Nachrichtentechnik | RWTH Aachen University" width="240px">

# MATLAB AudioDemo zur Vorlesung Grundgebiete der Elektrotechnik 3 &ndash; Signale und Systeme

Die Audio-Demo ermöglicht das Abspielen von `.wav`-Dateien in Echtzeit sowie das gleichzeitige Darstellen des Signals im Zeit-, im Frequenz- und im Zeit-Frequenz-Bereich. Desweiteren lassen sich eine Auswahl von Filtern sowie Unterabtastung auf das Signal anwenden.

Zur Installation wird MATLAB mit der DSP- und der Signal-Processing-Toolbox benötigt (zuletzt getestet mit MATLAB 2022b).
