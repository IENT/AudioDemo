classdef AudioDemo < handle
% AUDIODEMO Audio demonstrator
%
%   AudioDemo() starts audio demonstrator GUI. 
%   Displays audio in time, frequency and time-frequency domain. Audio is
%   either read from a .wav-file or generated with MATLAB.
%   
%   See also: Model, Viewer
  
  properties
    viewer;             % Viewer object
    model;              % Model object
    htimer = struct();  % Timer handles
    tr = [];            % Translator object
    settings = [];      % Settings from ini
  end
  
  
  methods
    %% Constructor
    % Constructor creates AudioDemo object and sets default parameter
    function obj = AudioDemo(audiofilename, iniFile)
      if nargin < 1, audiofilename = 'wav/abba.wav'; end
      if nargin < 2, iniFile = 'settings.ini'; end
      
      % Check MATLAB version
      r = version('-release'); r(end) = []; r = str2double(r);
      if r < 2014, error('Unsupported MATLAB release. I work only with R2014a and newer.'); end
      
      % Force UTF8 encoding
      feature('DefaultCharacterSet', 'UTF8');
      
      % Include external functions
      addpath('external');
      
      % Try to load settings
      obj.settings = IniConfig;
      if ~obj.settings.ReadFile(iniFile), warning(['Could not read ini file ' iniFile]); end
      
      % Timers
      obj.htimer.status = [];
      obj.htimer.getter = timer('BusyMode', 'drop',...
        'ExecutionMode', 'fixedSpacing', 'Name', 'getter',...
        'Period', obj.settings.GetValues('Timer','dataPeriod',0.01), ...
        'TimerFcn', @obj.updateData);
      
      obj.htimer.plotter = timer('BusyMode', 'drop',...
        'ExecutionMode','fixedRate', 'Name', 'plotter',...
        'Period', obj.settings.GetValues('Timer','plotPeriod',0.05), ...
        'TimerFcn', @obj.updatePlot);
      
      % Translator
      obj.tr = i18n(obj.settings.GetValues('General','language','de'));
      
      % Model and viewer objects
      obj.model = Model(obj, obj.tr);      
      obj.viewer = Viewer(obj, obj.tr);
      
      % Init
      [a,b,c] = fileparts(audiofilename);
      obj.loadFile([b c] ,[a '/']);
    end
    
    
    %% Update data
    % Triggers model to update data
    function updateData(obj,handle,event)
      try
        obj.model.updateData(handle,event);
      catch getErr
        disp('Error. Stopping getter. Please restart program.');
        disp(getErr.message);
        disp(getErr.stack(1))        
      end
    end
    
    
    %% Update plot
    % Triggers viewer to update plot
    function updatePlot(obj,handle,event) %#ok<INUSD> 
      try
        % Calculate data from model
        mode = obj.viewer.plotMode;
        [s] = obj.model.updatePlotData(mode);

        % Update plot in viewer
        obj.viewer.updatePlot(s);
      catch getErr
        disp('Error. Stopping plotter. Please restart program.');
        disp(getErr.message);
        disp(getErr.stack(1))        
      end
    end
    
    
    %% Play pause
    % Toggles between play and pause mode
    function playPause(obj)
      switch obj.viewer.hgui.playPause.String
        case 'Play'
%           b = datacursormode(obj.viewer.hgui.fig); b.removeAllDataCursors();
          start(obj.htimer.getter)
          start(obj.htimer.plotter)
          obj.viewer.hgui.playPause.String = 'Pause';
          obj.viewer.changePlotMode();
          obj.viewer.updateDecimation();
          obj.viewer.hgui.sourceFileLoad.Enable = 'off';
          
        case 'Pause'
          stop(obj.htimer.getter)
          stop(obj.htimer.plotter)
          obj.viewer.hgui.playPause.String = 'Play';
          obj.viewer.hgui.sourceFileLoad.Enable = 'on';
      end
    end
    
    
    %% Reset
    % Reset timer, model and viewer
    function reset(obj)
      stop(obj.htimer.getter);
      stop(obj.htimer.plotter);
      obj.model.reset();
      obj.viewer.reset();
      obj.updatePlot();
      obj.updateFilter();
    end
    
    
    %% Update filter
    function updateFilter(obj)
      Fs = obj.getFS();
      [filterMode,f0,f1] = obj.viewer.getFilterData(Fs);
      obj.model.setFilterData(filterMode,f0,f1);
      obj.viewer.updateFilterPlot();
    end
    
    %% Update Decimation
    function setDecimation(obj,dec)
      obj.model.data.decimation = dec;
      obj.model.data.downsampleShift = 0;
    end
    
    
    %% Update sampling frequency
    % Updates sampling frequency and publishes it to model and viewer
    function setFS(obj,Fs)
      % Update model
      obj.model.setFS(Fs);
      
      % Update viewer
      obj.viewer.updateFMax();
    end
    function Fs = getFS(obj)
      Fs = obj.model.getFS();
    end
    
    function setDataFetchMode(obj,fm)
      obj.model.data.dataFetchMode = fm;
    end
    
    
    %% Load new file
    % Prepares playback of new file
    function loadFile(obj,FileName,PathName)
      obj.viewer.hgui.playPause.Enable = 'off';
      
      % Load new file
      obj.model.setFilename([PathName FileName]);
      
      % Propagate sampling frequency
      Fs = obj.getFS();
      obj.viewer.displayFileInfo(FileName,Fs);
      obj.setFS(Fs);
      
      % Reset everything
      obj.reset();
      pause(0.5);
      obj.viewer.hgui.playPause.Enable = 'on';
    end
    
    
    %% Close request
    % Request deletes timers and closes model
		function CloseRequestHandler(obj, handles, event) %#ok<INUSD>      
      
      % Delete timers
      fn = fieldnames(obj.htimer);
      for iter=1:length(fn)
        t = obj.htimer.(fn{iter});
        % Stop timer
        if ~isempty(t) && strcmp(t.Running,'on')
          stop(t);
        end
        
        % Delete timer
        delete(t);
      end
      
      obj.model.CloseRequestHandler();
    end


  end % methods
end % AudioDemo