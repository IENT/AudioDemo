classdef i18n < handle
  properties (GetAccess = 'private')
    DB = {
      'title'     'AudioDemo'  'AudioDemo'
      'volume'    'Input volume'   'Lautstärke Eingang'
      'settings'      'Settings'    'Einstellungen'
      'select'        'Select'      'Auswählen'
      'save'          'Save'        'Speichern'
      'cancel'        'Cancel'      'Abbrechen'
      'close'         'Close'       'Schließen'
      'reset'         'Reset'       'Zurücksetzen'
      'load'          'Load'        'Laden'
      'file'          'File'        'Datei'
      'selectLang'    'Select language' 'Sprache auswählen'
      'type'   'Type' 'Typ'
      'plot'   'Plot' 'Plot'
      'activate'   'Activate' 'Aktivieren'
      'centfreq'   'Center frequency [Hz]' 'Mittenfrequenz [Hz]'
      'maxfreq'    'Maximal frequency [Hz]' 'Maximal- frequenz [Hz]'
      'cutfreq'    'Cutoff frequency [Hz]' 'Grenzfrequenz [Hz]'
      'freq'  'Frequency [kHz]' 'Frequenz [kHz]'
      'ampl'  'Amplitude' 'Amplitude'
      'fs'  'sampling frequency' 'Abtastfrequenz'
      'time'  'Time [s]' 'Zeit [s]'
      'bandwidth'  'Bandwidth [Hz]'  'Bandbreite [Hz]'
      'gen'   'Generator'   'Generator'
      'filt'      'Filter'    'Filter'
      'filt:lp'   'Lowpass'   'Tiefpass'
      'filt:hp'   'Highpass'  'Hochpass'
      'filt:bp'   'Bandpass'  'Bandpass'
      'filt:hrtf'   'HRTF'  'HRTF'
      'filt:comb'   'Comb filter'  'Kammfilter'
      'filt:crazy'   'Test'  'Test'
      'filt:wah'   'Wah'  'Wah'
      'sig:const'     'Constant'    'Konstante'
      'sig:tri'       'Triangle'    'Dreieck'
      'sig:sine'      'Sine'        'Sinus'
      'sig:rect'      'Rect'        'Rechteck'
      'sig:data'      'Data signal' 'Datensignal'
      'sig:noise'     'Noise' 'Rauschen'
      'sig:saw'       'Sawtooth'    'Sägezahn'
      'ctrl'   'Control'   'Control'
      'repeat'   'Repeat'   'Repeat'
      'subsamp'   'Sub-sampling'   'Unterabtastung'
      'subsampfac'   'Sampling factor'   'Abtastfaktor'
      'loudnessComp' 'Loudness compensation' 'Lautstärke kompensieren'
      'originalfs' 'Original sampling frequency' 'Originalabtastfreq.'
      'newfs' ' New sampling frequency' 'Neue Abtastfreq.'
      'about',    'About', 'Über'
      'none'        'None'  'Null'
      'pl' 'Plot' 'Plot'
      'pl:specgr' 'Spectrogram' 'Spektrogramm'
      'pl:spectr' 'Spectrum' 'Spektrum'
      'pl:time' 'Time signal' 'Zeitsignal'
      'timesignal' 'Time-domain signal' 'Zeitsignal'
      'Panning' 'Panning' 'Panning'
      'mix'   'Mix'   'Mix'
    };
    
  end
  properties (GetAccess = 'public')
    lang = 'de';
    langs = {'en','de'};
  end
  
  methods
    %% Constructor
    function obj = i18n(lang)
      if nargin == 1
        obj.setLang(lang);
      end
    end
    
    %% Set language
    function obj = setLang(obj,l)
      mask = ismember(obj.langs,l);
      if ~any(mask), error('Unknown language'); end
      obj.lang = l;
    end
    
    %% Find translation
    function translatedStr = translate(obj,str,lang)
      
      if nargin == 3, obj.lang = lang; end

      % Find ID of language
      langID = find(ismember(obj.langs,obj.lang))+1;
      if isempty(langID), langID = 3; end

      % Find string in DB
      mask = ismember(obj.DB(:,1),str);

      if sum(mask) == 1
        translatedStr = obj.DB{mask,langID};
      else
        % If string was not found, return first string
        translatedStr = [upper(str(1)),str(2:end)];
        warning(['i18n: Could not find translation for ' str]);
      end
    end
    
    
    %% Overload () operator
    function B = subsref(obj,A)
      switch A(1).type
        case '()'
          B = obj.translate(A.subs{:});
        otherwise
          B = builtin('subsref', obj, A);
          % TODO: check for nargout of function to be called here
      end
    end
  end
end