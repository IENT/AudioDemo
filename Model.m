classdef Model < handle
% MODEL hold data for AudioDemo
%
% See also: AudioDemo, Viewer

  properties
    controller; % link to controller object
    data = struct();   % data structure
    haudio = struct(); % audio handles
    tr = []; % Translator object
    legacy_mr = 0; % MATLAB version
  end
  
  methods
    %% Constructor
    % Creates new model object
    function obj = Model(ctr, tr)
      obj.controller = ctr;
      obj.tr = tr;
      
      % Get parameters from settings
      bufferSize = obj.controller.settings.GetValues('Audio','buffersize',2048);
      timeSize = obj.controller.settings.GetValues('Audio','stftTimeFrames',40);
      deviceName = obj.controller.settings.GetValues('Audio','device','Default');
      queueDuration = obj.controller.settings.GetValues('Audio','queueDuration',0.2);
      
      % Initialize
      obj.data.stream = zeros(bufferSize,2);
      obj.data.streamBuffer = zeros(bufferSize*4,1);
      NI = obj.getNyquistIndex();
      obj.data.streamSTFT = zeros(NI,timeSize);
      obj.data.w = hann(size(obj.data.streamBuffer,1));
     
      obj.data.dataFetchMode = obj.tr('sig:sine');
      obj.data.phase = 0;
      obj.data.b = []; obj.data.a = [];  obj.data.filterLast = [];
      obj.data.downsampleShift = 0;
      obj.data.updateVol = 0;
      obj.data.numObjects = 1;
      
      % Initialize uniformed partitioned convolution
      room = 'stairway'; % room = 'aula';
      obj.data.up = upConvOld(bufferSize,room);
      
      % Audio objects
      obj.haudio.reader = dsp.AudioFileReader('wav/abba.wav','SamplesPerFrame',bufferSize);
      
      r = version('-release'); r(end) = []; r = str2double(r);
      if r > 2018
        obj.haudio.player = audioDeviceWriter('SampleRate', obj.haudio.reader.SampleRate, ...
            'Device',deviceName, 'BufferSize', bufferSize);
        obj.haudio.recorder = audioDeviceReader('NumChannels', 2,'SamplesPerFrame',bufferSize,...
            'SampleRate', obj.haudio.reader.SampleRate, 'Device',deviceName); 
        setup(obj.haudio.recorder);
      else
        obj.haudio.player = dsp.AudioPlayer('OutputNumUnderrunSamples',1, ...
            'QueueDuration',queueDuration,'DeviceName',deviceName);
        obj.haudio.player.SampleRate = obj.haudio.reader.SampleRate;
        obj.haudio.recorder = dsp.AudioRecorder('NumChannels', 2,'SamplesPerFrame',bufferSize,...
            'OutputNumOverrunSamples',1,'QueueDuration',queueDuration,'DeviceName',deviceName);
        obj.haudio.recorder.SampleRate = obj.haudio.reader.SampleRate;
      end
      obj.legacy_mr = r;
    end
    
    
    %% Update data
    % Updates data buffer and filteres content of buffer
    function updateData(obj,handle,event) %#ok<INUSD> 
      % Fetch data
      in = obj.fetchData(); 

      % Downsample data
      sample_factor = obj.data.decimation;
      if sample_factor > 1
        rec_data = zeros(size(in));
        in = downsample(in,sample_factor,obj.data.downsampleShift);
        rec_data(obj.data.downsampleShift+1:sample_factor:end,:) = in;
        in = ((sample_factor-1)*obj.controller.viewer.hgui.compDecimation.Value+1)*rec_data;
        obj.data.downsampleShift = obj.data.downsampleShift + sample_factor - mod(size(in,1),sample_factor);
        obj.data.downsampleShift = mod(obj.data.downsampleShift,sample_factor);
      end
      
      % Filter each object      
      if obj.data.updateVol > 0 % apply filter with old states
        [inOld,obj.data.oldfilterLast] = obj.applyFilter(in,obj.data.oldb,obj.data.olda,obj.data.oldfilterLast);
      end
      
      % Filter with current states
      [in,obj.data.filterLast] = obj.applyFilter(in,obj.data.b,obj.data.a,obj.data.filterLast);
      
      if obj.data.updateVol > 0 && exist('inOld','var') % add old filter output to new filter output
        in = in*(1-obj.data.updateVol)+inOld*obj.data.updateVol;
        if obj.data.flags.filterUpdated % only if filter is updated, volume is decreased
          obj.data.updateVol = obj.data.updateVol-0.2; 
          if obj.data.updateVol < eps, obj.data.updateVol = 0; end
        end
      end
      
      % HRTF / stereo panning
      % to ensure obj.data.stream is always stereo
      vol  = obj.controller.viewer.panning.getObjectVolume();
      if obj.controller.viewer.panning.hgui.hrtf.Value
        % Use uniformly partitioned convolution for HRTF
        obj.data.stream = obj.data.up.conv(in,vol);
      else
        % Stereo panning
        Nobjects = length(vol);
        angs = obj.controller.viewer.panning.getObjectAngles()*pi/180/2; % to rad and from 0 to 90 degrees

        obj.data.stream = zeros(size(obj.data.stream,1),2);
        for n=1:min(length(vol),size(in,2))
          obj.data.stream(:,1) = obj.data.stream(:,1) + vol(n)*sin(angs(n))*in(:,n);
          obj.data.stream(:,2) = obj.data.stream(:,2) + vol(n)*cos(angs(n))*in(:,n);
        end
        obj.data.stream = obj.data.stream / Nobjects;
      end
      
      % Buffer for plot
      obj.data.streamBuffer = [obj.data.streamBuffer(size(obj.data.stream,1)+1:end);mean(obj.data.stream,2)];
      
      % Write data to soundcard
      nUnderrun = step(obj.haudio.player, obj.data.stream);
      if nUnderrun
        if obj.legacy_mr > 2018
          dn = obj.haudio.player.Device;
        else
          dn = obj.haudio.player.DeviceName;
        end
        disp(['Output ' dn ' values dropped: ' num2str(nUnderrun)]);
      end
      
      % Reader finished
      if obj.haudio.reader.isDone
        reset(obj.haudio.reader)
        if ~obj.controller.viewer.getRepeat()
          obj.data.stream = zeros(size(obj.data.stream));
          obj.controller.playPause();
        end
      end
    end
    
    
    %% Apply filter
    function [in,fl] = applyFilter(obj,in,b,a,fl)
      filtActive = obj.controller.viewer.getFilterActive();
      if filtActive %&& obj.data.flags.filterUpdated
        Nobjects = obj.getNumObjects();
        mask = obj.controller.viewer.panning.getObjectFX();
        vol  = obj.controller.viewer.panning.getObjectVolume();
        
        for it=1:Nobjects
          if mask(it) && vol(it) > 0 % filter only if active and hearable
            if ~obj.data.flags.crazysel
              [in(:,it),fl{it}] = ...
                filter(b,a,in(:,it),fl{it});
              if any(any(isnan(fl{it}))), fl{it} = zeros(size(fl{it})); end
            else % distortion
              gain = 100; mix = 0.9;
              q = in(:,it)*gain;
              z = sign(q).*(1-exp(-abs(q)));
              z = z/8;
              in(:,it) = mix*z+(1-mix)*in(:,it);
            end
          end
        end
                
      end     
    end
    
    %% Fetch data from generator
    % Generates synthesized data
    function s = fetchData(obj)
      L = size(obj.data.stream,1);
      switch obj.data.dataFetchMode
        case obj.tr('sig:sine')
          Fs = obj.getFS();
          [amp,freq] = obj.controller.viewer.getGeneratorData(Fs);
          phaseInc = 2*pi*freq/Fs;
          phaseVec = (obj.data.phase + (1:L).*phaseInc)';
          phaseVec = mod(phaseVec,2*pi);
          obj.data.phase = phaseVec(end);
          s = amp*sin(phaseVec);
          
        case obj.tr('sig:saw')
          Fs = obj.getFS();
          [amp,freq] = obj.controller.viewer.getGeneratorData(Fs);
          phaseInc = 2*pi*freq/Fs;
          phaseVec = (obj.data.phase + (1:L).*phaseInc)';
          phaseVec = mod(phaseVec,2*pi);
          obj.data.phase = phaseVec(end);
          s = amp*sawtooth(phaseVec);
          
        case obj.tr('sig:noise')
          Fs = obj.getFS();
          [amp] = obj.controller.viewer.getGeneratorData(Fs);
          s = amp*randn(L,1)*0.5;
          
        case 'Reader'
          s = step(obj.haudio.reader);
          
        case 'Recorder'
          vol = obj.controller.viewer.getRecVolume();
          s = vol*step(obj.haudio.recorder);
      end
    end
    
    
    %% Update plot data
    % Generates FFT or STFT for display
    function s = updatePlotData(obj,mode)
      switch mode
        case obj.tr('pl:specgr') % spectrogram
          % FFT
          tmp = fft(obj.data.streamBuffer(:,1).*obj.data.w);
          NI = obj.getNyquistIndex();
          tmp = abs(tmp(1:NI));
          
          % Update spectrogram matrix
          obj.data.streamSTFT(:,1) = [];
          obj.data.streamSTFT(:,end+1) = tmp.^(1/2);
          s = obj.data.streamSTFT;
        case obj.tr('pl:spectr') % spectrum
          tmp = fft(obj.data.streamBuffer(:,1).*obj.data.w);
          NI = obj.getNyquistIndex();
          
          s = abs(tmp(1:NI)).^(1/2);
        case obj.tr('pl:time')
          s = obj.data.streamBuffer;
      end
    end
    
    
    %% Set filter data
    % Updates filter data
    function setFilterData(obj,filterMode,f0,f1)
      obj.data.updateVol = 1;
      obj.data.oldb = obj.data.b; obj.data.olda = obj.data.a; obj.data.oldfilterLast = obj.data.filterLast;
      obj.data.flags.filterUpdated = 0; 
      obj.data.flags.crazysel = 0;
      Fs = obj.getFS();
      
      % Create filter
      switch filterMode
        case obj.tr('filt:lp') % Lowpass
          [obj.data.b,obj.data.a] = butter(10,f0/(Fs/2),'low'); obj.data.a = obj.data.a';obj.data.b = obj.data.b';
        case obj.tr('filt:hp') % Highpass
          [obj.data.b,obj.data.a] = butter(10,f0/(Fs/2),'high'); obj.data.a = obj.data.a';obj.data.b = obj.data.b';
        case obj.tr('filt:comb') % comb
          fo = f0;  q = round(f1); bw = (fo/(Fs/2))/q;
          [obj.data.b,obj.data.a] = iircomb(round(Fs/fo),bw,'notch'); obj.data.a = obj.data.a';obj.data.b = obj.data.b';% Note type flag 'notch'
        case obj.tr('filt:bp') % bandpass
          fc = f0;
          bw = f1;
          f0 = fc-bw/2;
          f1 = fc+bw/2;
          [obj.data.b,obj.data.a] = butter(6,[f0 f1]/(Fs/2),'bandpass'); obj.data.a = obj.data.a';obj.data.b = obj.data.b';
        case obj.tr('filt:crazy') % crazy test
          obj.data.flags.crazysel = 1;
          
        case obj.tr('filt:wah')
          wah = f0;
          g = 0.005*4^wah; % overall gain for second-order s-plane transfer funct.
          fr = 450*2^(2.3*wah); % resonance frequency (Hz) for the same transfer funct.
          Q = 2^(2*(1-wah)+1); % resonance quality factor for the same transfer funct.
          frn = fr/Fs;
          R = 1 - pi*frn/Q; % pole radius
          theta = 2*pi*frn; % pole angle
          a1 = -2.0*R*cos(theta); % biquad coeff
          a2 = R*R; % biquad coeff
          obj.data.a = [1 a1 a2];
          obj.data.b = sum(obj.data.a);
      end
      
      % Initialize last filter states
      siz = [max(length(obj.data.b),length(obj.data.a))-1,1];
      Nobjects = obj.getNumObjects();
      obj.data.filterLast = cell(1,Nobjects);
      for it=1:Nobjects
        obj.data.filterLast{it} = zeros(siz);
      end
      obj.data.flags.filterUpdated = 1;
    end
    
    
    %% Update sampling frequency
    % Sets sampling frequency of player
    function setFS(obj,Fs)
      if nargin < 2
        Fs = obj.haudio.reader.SampleRate;
      end
      obj.haudio.player.SampleRate = Fs;
    end
    
    
    %% Get sampling frequency
    % Gets sampling frequency of player
    function Fs = getFS(obj)
      Fs = obj.haudio.player.SampleRate;
    end
       
    %% Get Nyquist index
    function NI = getNyquistIndex(obj)
      NI = round(size(obj.data.streamBuffer,1)/2)+1;
    end
    
    %% Get number of objects
    function N = getNumObjects(obj)
      switch obj.data.dataFetchMode
        case 'Reader'
          N = obj.data.numObjects;
        otherwise
          N = 1; % Everything else is mono for now
      end
    end
    
    %% Set filename
    function setFilename(obj,fn)
      obj.release();
      obj.haudio.reader.Filename = fn;
      obj.haudio.player.SampleRate = obj.haudio.reader.SampleRate;
      obj.data.dataFetchMode = 'Reader';
      obj.data.numObjects = obj.haudio.reader.info.NumChannels;
    end
    
    
    %% Close
    % Deletes audio devices
    function CloseRequestHandler(obj)
      obj.release();
      % Delete audio devices
      delete(obj.haudio.player);
      delete(obj.haudio.reader);
    end
    
    
    %% Reset
    % Resets audio devices and buffers
    function reset(obj)
      % Reset audio devices
      reset(obj.haudio.reader);
      reset(obj.haudio.player);
      reset(obj.haudio.recorder);
      
      % Reset buffers
      obj.data.stream = zeros(size(obj.data.stream));
      obj.data.streamSTFT = zeros(size(obj.data.streamSTFT));
      obj.data.streamBuffer = zeros(size(obj.data.streamBuffer));
    end
    
    
    %% Release
    % Releases audio devices
    function release(obj)
      release(obj.haudio.reader);
      release(obj.haudio.player);
      release(obj.haudio.recorder);
    end

  end % methods
end % Model

